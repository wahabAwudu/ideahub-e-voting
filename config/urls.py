from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^', include('evoting.apps.general.auth_user.urls', namespace='auth_user')),
    url(r'^vote/', include('evoting.apps.general.voting.urls', namespace='vote')),
    url(r'^dashboard/results/', include('evoting.apps.general.results.urls', namespace='result')),
    url(r'^dashboard/home/', TemplateView.as_view(template_name='index.html'), name='home'),
    url(r'^dashboard/students/', include('evoting.apps.general.students.urls', namespace='student')),
    url(r'^dashboard/presidents/', include('evoting.apps.portfolios.presidents.urls', namespace='president')),
    url(r'^dashboard/organizers/', include('evoting.apps.portfolios.organizers.urls', namespace='organizer')),
    url(r'^dashboard/finance-officers/', include('evoting.apps.portfolios.finofficers.urls', namespace='finofficer')),
    url(r'^dashboard/treasurers/', include('evoting.apps.portfolios.treasurers.urls', namespace='treasurer')),
    url(r'^dashboard/general-secretaries/', include('evoting.apps.portfolios.gensecretaries.urls', namespace='gensecretary')),
    url(r'^dashboard/pros/', include('evoting.apps.portfolios.pros.urls', namespace='pro')),
    url(r'^dashboard/wocoms/', include('evoting.apps.portfolios.wocoms.urls', namespace='wocom')),

    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
