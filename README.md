# ideahub-e-voting
this is to solve voting issues in our educational institutions

* install virtualenv and activate it
* create a postgresql database 'CREATE DATABASE evotingdb;' 
* migrate the models of the project using 'python manage.py migrate'
* install requirements by 'pip install -r requirements/local.txt'
* start the server by runing 'python manage.py runserver'
