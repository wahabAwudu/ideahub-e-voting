# django version 1.10
django==1.11.10

# django rest framework
djangorestframework==3.8.2

# pillow for images
Pillow==5.2.0

# django allauth for user authentication
django-allauth==0.36.0

# django crispy forms
django-crispy-forms==1.7.2

# django environ
django-environ==0.4.4

# postgresql file
psycopg2-binary==2.7.5

# authentications mixins
django-braces==1.13.0

# django storages for aws s3
django-storages==1.6.6

# whitenoise for staticfiles
whitenoise==3.3.1

# django braces mixins
django-braces==1.13.0

# django cors
django-cors-headers==2.2.0

# python decouple for environmental variables
python-decouple==3.1

# django redis and redis
django-redis==4.9.0
redis==2.10.6
