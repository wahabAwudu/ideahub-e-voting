from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class VoteManager(models.Manager):
    def get_votes_by_instance(self, instance):
        content_type = ContentType.objects.get_for_model(instance.__class__)
        object_id = instance.id
        return super(VoteManager, self).filter(portfolio=content_type, portfolio_object_id=object_id)


class Vote(models.Model):
    portfolio = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    portfolio_object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("portfolio", "portfolio_object_id")
    votes = models.PositiveIntegerField(default=0, blank=True)
    casted_at = models.DateTimeField(auto_now_add=True)

    objects = VoteManager()

    def __str__(self):
        return str(self.votes)
