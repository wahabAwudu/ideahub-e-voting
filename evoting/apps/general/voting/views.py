from django.shortcuts import render, get_object_or_404, redirect

from evoting.apps.portfolios.finofficers.models import FO
from evoting.apps.portfolios.gensecretaries.models import GenSecretary
from evoting.apps.portfolios.organizers.models import Organizer
from evoting.apps.portfolios.presidents.models import President
from evoting.apps.portfolios.pros.models import PRO
from evoting.apps.portfolios.treasurers.models import Treasurer
from evoting.apps.portfolios.wocoms.models import Wocom

from evoting.apps.general.voting.models import Vote
from evoting.apps.general.voting.forms import VotingForm


def wocom_voting_view(request):
    # if not request.user.is_authenticated:
    #     return redirect('auth_user:login')
        
    wocom_list = Wocom.objects.filter(status="AC")
    form = VotingForm(request.POST or None)

    portfolio_id = request.POST.get("contestant_id")
    p_id = 0
    try:
        p_id = int(portfolio_id)
    except:
        p_id = 0
        
    print("id:" + str(p_id))

    if form.is_valid():
        instance = get_object_or_404(Wocom, id=p_id)
        print("instance id:" + str(instance.id))
        Vote.objects.get_or_create(
            portfolio=instance.get_content_type,
            portfolio_object_id=instance.pk,
            votes=1
        )
        return redirect('auth_user:logout')

    page_title = "Wocoms | Voting"
    header_title = "Vote for a Wocom"
    
    context = {
        'page_title': page_title,
        'object_list': wocom_list,
        'header_title': header_title,
        'form': form
    }
    return render(request, "admin/portfolios/voting.html", context)


def treasurer_voting_view(request):
    treasurer_list = Treasurer.objects.filter(status="AC")
    form = VotingForm(request.POST or None)

    portfolio_id = request.POST.get("contestant_id")
    p_id = 0
    try:
        p_id = int(portfolio_id)
    except:
        p_id = 0
        
    print("id:" + str(p_id))

    if form.is_valid():
        instance = get_object_or_404(Treasurer, id=p_id)
        print("instance id:" + str(instance.id))
        Vote.objects.get_or_create(
            portfolio=instance.get_content_type,
            portfolio_object_id=instance.pk,
            votes=1
        )
        return redirect('vote:pro')

    page_title = "Treasurers | Voting"
    header_title = "Vote for a Treasurer"
    
    context = {
        'page_title': page_title,
        'object_list': treasurer_list,
        'header_title': header_title,
        'form': form
    }
    return render(request, "admin/portfolios/voting.html", context)



def pro_voting_view(request):
    pro_list = PRO.objects.filter(status="AC")
    form = VotingForm(request.POST or None)

    portfolio_id = request.POST.get("contestant_id")
    p_id = 0
    try:
        p_id = int(portfolio_id)
    except:
        p_id = 0
        
    print("id:" + str(p_id))

    if form.is_valid():
        instance = get_object_or_404(PRO, id=p_id)
        print("instance id:" + str(instance.id))
        Vote.objects.get_or_create(
            portfolio=instance.get_content_type,
            portfolio_object_id=instance.pk,
            votes=1
        )
        return redirect('vote:organizer')

    page_title = "PROS | Voting"
    header_title = "Vote for a PRO"
    
    context = {
        'page_title': page_title,
        'object_list': pro_list,
        'header_title': header_title,
        'form': form
    }
    return render(request, "admin/portfolios/voting.html", context)


def president_voting_view(request):
    president_list = President.objects.filter(status="AC")
    form = VotingForm(request.POST or None)

    portfolio_id = request.POST.get("contestant_id")
    p_id = 0
    try:
        p_id = int(portfolio_id)
    except:
        p_id = 0
        
    print("id:" + str(p_id))

    if form.is_valid():
        instance = get_object_or_404(President, id=p_id)
        print("instance id:" + str(instance.id))
        Vote.objects.get_or_create(
            portfolio=instance.get_content_type,
            portfolio_object_id=instance.pk,
            votes=1
        )
        return redirect('vote:general_secretary')

    page_title = "Presidents | Voting"
    header_title = "Vote for a President"
    
    context = {
        'page_title': page_title,
        'object_list': president_list,
        'header_title': header_title,
        'form': form
    }
    return render(request, "admin/portfolios/voting.html", context)



def organizer_voting_view(request):
    organizer_list = Organizer.objects.filter(status="AC")
    form = VotingForm(request.POST or None)

    portfolio_id = request.POST.get("contestant_id")
    p_id = 0
    try:
        p_id = int(portfolio_id)
    except:
        p_id = 0
        
    print("id:" + str(p_id))

    if form.is_valid():
        instance = get_object_or_404(Organizer, id=p_id)
        print("instance id:" + str(instance.id))
        Vote.objects.get_or_create(
            portfolio=instance.get_content_type,
            portfolio_object_id=instance.pk,
            votes=1
        )
        return redirect('vote:wocom')

    page_title = "Organizers | Voting"
    header_title = "Vote for an Organizer"
    context = {
        'page_title': page_title,
        'object_list': organizer_list,
        'header_title': header_title,
        'form': form
    }
    return render(request, "admin/portfolios/voting.html", context)


def gensec_voting_view(request):
    gensec_list = GenSecretary.objects.filter(status="AC")
    form = VotingForm(request.POST or None)

    portfolio_id = request.POST.get("contestant_id")
    p_id = 0
    try:
        p_id = int(portfolio_id)
    except:
        p_id = 0
        
    print("id:" + str(p_id))

    if form.is_valid():
        instance = get_object_or_404(GenSecretary, id=p_id)
        print("instance id:" + str(instance.id))
        Vote.objects.get_or_create(
            portfolio=instance.get_content_type,
            portfolio_object_id=instance.pk,
            votes=1
        )
        form = VotingForm()
        return redirect('vote:finance_officer')

    page_title = "General Secretaries | Voting"
    header_title = "Vote for a General Secretary"
    
    context = {
        'page_title': page_title,
        'object_list': gensec_list,
        'header_title': header_title,
        'form': form
    }
    return render(request, "admin/portfolios/voting.html", context)


# finance officers voting view
def fo_voting_view(request):
    fo_list = FO.objects.filter(status="AC")
    form = VotingForm(request.POST or None)

    portfolio_id = request.POST.get("contestant_id")
    p_id = 0
    try:
        p_id = int(portfolio_id)
    except:
        p_id = 0
        
    print("id:" + str(p_id))

    if form.is_valid():
        instance = get_object_or_404(FO, id=p_id)
        print("instance id:" + str(instance.id))
        Vote.objects.get_or_create(
            portfolio=instance.get_content_type,
            portfolio_object_id=instance.pk,
            votes=1
        )
        return redirect('vote:treasurer')

    page_title = "Finance Officers | Voting"
    header_title = "Vote for a Finance Officer"
    context = {
        'page_title': page_title,
        'object_list': fo_list,
        'header_title': header_title,
        'form': form
    }
    return render(request, "admin/portfolios/voting.html", context)
