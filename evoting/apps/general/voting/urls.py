from django.conf.urls import url
from .views import (
    fo_voting_view,
    gensec_voting_view,
    organizer_voting_view,
    president_voting_view,
    pro_voting_view,
    treasurer_voting_view,
    wocom_voting_view
)


urlpatterns = [
    url(r'^finance-officer/$', fo_voting_view, name='finance_officer'),
    url(r'^general-secretary/$', gensec_voting_view, name='general_secretary'),
    url(r'^organizer/$', organizer_voting_view, name='organizer'),
    url(r'^president/$', president_voting_view, name='president'),
    url(r'^pro/$', pro_voting_view, name='pro'),
    url(r'^treasurer/$', treasurer_voting_view, name='treasurer'),
    url(r'^wocom/$', wocom_voting_view, name='wocom'),
]
