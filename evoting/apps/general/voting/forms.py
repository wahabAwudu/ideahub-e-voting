from django import forms
from .models import Vote


class VotingForm(forms.ModelForm):
    votes = forms.IntegerField(widget=forms.TextInput(attrs={'hidden': True}), required=False)

    class Meta:
        model = Vote
        fields = ['votes']
