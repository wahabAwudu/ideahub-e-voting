from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey


class VerifyManager(models.Manager):
    def get_verification_by_instance(self, instance):
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return super(VerifyManager, self).filter(student_voter=content_type, student_voter_object_id=instance.id)


class Verify(models.Model):
    student_voter = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    student_voter_object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey("student_voter", "student_voter_object_id")
    verified = models.BooleanField()
    verified_at = models.DateTimeField(auto_now_add=True)

    objects = VerifyManager()

    def __str__(self):
        return str(self.verified)
