from django import forms
from .models import Verify


class VerifyForm(forms.ModelForm):
    class Meta:
        fields = ['verified']
        model = Verify
