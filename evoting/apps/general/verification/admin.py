from django.contrib import admin

from .models import Verify


class VerifyAdmin(admin.ModelAdmin):
    model = Verify
    list_display = ['__str__']


admin.site.register(Verify, VerifyAdmin)
