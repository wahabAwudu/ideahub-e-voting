from django.apps import AppConfig


class SessionsAuthConfig(AppConfig):
    name = 'auth_user'
