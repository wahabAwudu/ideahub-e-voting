from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth import authenticate

User = get_user_model()


class UserRegisterForm(forms.ModelForm):
    username = forms.CharField(max_length=30, label='index_number')

    class Meta:
        model = User
        fields = ['username']

    def clean(self):
        index_number = self.cleaned_data.get('username')

        if User.objects.filter(username=index_number).exists():
            raise forms.ValidationError("Index number already Exists")

        return super(UserRegisterForm, self).clean()


class UserLoginForm(forms.Form):
    index_number = forms.CharField(max_length=30, label='index_number')

    def clean(self):
        index_number = self.cleaned_data.get('index_number')
        password = "default"
        authenticate_user = authenticate(username=index_number, password=password)

        if not authenticate_user:
            raise forms.ValidationError("You Index Number is not registered")
        return super(UserLoginForm, self).clean()
