from django.shortcuts import render, redirect, HttpResponse, get_object_or_404
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import get_user_model, login, logout, authenticate
from .forms import UserLoginForm, UserRegisterForm

User = get_user_model()


def signup_view(request):
    if not request.user.is_authenticated and not request.user.is_superuser:
        return HttpResponse("Login As Admin First and then continue")

    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        index_number = instance.username
        password = "default"
        instance.set_password(password)
        instance.save()
        return redirect('auth_user:register')

    title = "SignUp | IdeaHub E-voting"
    header_title = "SignUp To Begin Voting"
    context = {
        'form': form,
        'title': title,
        'header_title': header_title
    }

    return render(request, 'register/auth_form.html', context)


def login_view(request):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        index_number = request.POST.get('index_number')

        if User.objects.filter(username=index_number, verified=False, voted=False):
            return HttpResponse("You have not been verified and therefore cannot vote")

        elif User.objects.filter(username=index_number, verified=True, voted=False):
            password = "default"
            authenticate_user = authenticate(request, username=index_number, password=password)
            login(request, authenticate_user)
            return reverse_lazy('vote:president')

        elif User.objects.filter(username=index_number, verified=True, voted=True):
            return HttpResponse("You have already voted for all Portfolios")

    title = "Login | IdeaHub E-voting"
    header_title = "Login To Begin Voting"
    context = {
        'form': form,
        'title': title,
        'header_title': header_title
    }

    return render(request, 'register/auth_form.html', context)


def logout_view(request):
    logout(request)
    return HttpResponse("Thank you for Voting")

