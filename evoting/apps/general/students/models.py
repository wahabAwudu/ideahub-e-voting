from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save
from django.utils.text import slugify
from django.utils.encoding import python_2_unicode_compatible
from datetime import datetime

@python_2_unicode_compatible
class User(AbstractUser):
    othernames = models.CharField(max_length=150, blank=True, null=True)
    verified = models.BooleanField(default=False)
    verified_at = models.DateTimeField(null=True, blank=True)
    voted = models.BooleanField(default=False)
    voted_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('student:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.username

    @property
    def get_content_type(self):
        return ContentType.objects.get_for_model(self.__class__)

    # @property
    # def verification(self):
    #     return Verify.objects.get_verification_by_instance(self)

#
# def create_verification(sender, instance, created, **kwargs):
#     if created:
#         Verify.objects.create(
#             student_voter=instance.get_content_type,
#             student_voter_object_id=instance.pk,
#             verified=False
#         )
#
#
# post_save.connect(create_verification, sender=User)
