from django.contrib import admin
from django import forms
# from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth.admin import UserAdmin

from .models import User


class MyCustomUserAdmin(admin.ModelAdmin):
    model = User
    list_display = ['__str__', 'voted']


admin.site.register(User, MyCustomUserAdmin)


# class MyUserChangeForm(UserChangeForm):
#     username = forms.RegexField(
#         label="Index Number",
#         max_length=30,
#         regex=r'^[\w-]+$',  # even as \d+
#         help_text=
#         'Required. 30 characters or fewer. Alphanumeric characters only (letters, digits, hyphens and underscores)'
#     )
#
#
# class MyUserCreationForm(UserCreationForm):
#     username = forms.RegexField(
#         label="Index Number",
#         max_length=30,
#         regex=r'^[\w-]+$',  # same as \d+
#         help_text=
#         'Required. 30 characters or fewer. Alphanumeric characters only (letters, digits, hyphens and underscores)'
#     )
#
#
# class MyUserAdmin(UserAdmin):
#     form = MyUserChangeForm
#     add_form = MyUserCreationForm
#
#
# admin.site.register(User, MyUserAdmin)
