from django.shortcuts import redirect, get_object_or_404, render, HttpResponse
from django.core.urlresolvers import reverse_lazy, reverse
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView
from django.contrib.auth import get_user_model

from .forms import IndexNUmberForm

User = get_user_model()


class StudentVoterListView(ListView):
    model = User
    template_name = "admin/students/list.html"

    def get_queryset(self):
        return self.model.objects.all().order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "List of Student Voters"
        context['unique_url'] = "students"
        return context


def student_voter_detail_view(request, pk=None):
    instance = get_object_or_404(User, pk=pk)
    # form = VerifyForm(request.POST or None)
    #
    # if form.is_valid():
    #     Verify.objects.get_or_create(
    #         verified=form.cleaned_data.get('verified')
    #     )
    #     form = VerifyForm()

    page_title = "Student Voter"
    unique_url = "students"
    # verified = instance.verification
    # if not verified:
    #     verified = "No"
    # else:
    #     verified = instance.verification
    context = {
        'object': instance,
        # 'form': form,
        'page_title': page_title,
        'unique_url': unique_url,
        # 'verified': verified,
        # 'verification': instance.verification,
    }

    return render(request, 'admin/students/detail.html', context)


class StudentVoterCreateView(CreateView):
    model = User
    template_name = "admin/students/form.html"
    fields = [
        'username',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_header'] = "Add New Student Voter"
        context['unique_url'] = "students"
        return context


class StudentVoterUpdateView(UpdateView):
    model = User
    template_name = "admin/students/form.html"
    fields = [
        'username',
    ]

    def get_context_data(self, **kwargs):
        context = super(StudentVoterUpdateView, self).get_context_data(**kwargs)
        context['page_header'] = "Update Student Voter"
        context['unique_url'] = "students"
        return context


class StudentVoterDeleteView(DeleteView):
    model = User
    template_name = "admin/students/confirm_delete.html"
    success_url = reverse_lazy('student:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Delete Student Voters"
        context['unique_url'] = "students"
        return context


# def index_number_view(request):
#     if not request.user.is_authenticated and not request.user.is_superuser:
#         return HttpResponse("Login As Admin First and then continue")

#     form = IndexNUmberForm(request.POST or None)
#     if form.is_valid():
#         index_number = request.POST.get('index_number')
#         instance = get_object_or_404(User, username=index_number)
#         return reverse_lazy('student:detail', kwargs={'pk': instance.pk})

#     page_header = "Index Number"
#     context = {
#         'form': form,
#         'page_header': page_header
#     }

#     return render(request, 'admin/students/form.html', context)
