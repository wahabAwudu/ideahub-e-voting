from django import forms
from django.contrib.auth import get_user_model

User = get_user_model()


class IndexNUmberForm(forms.Form):
    index_number = forms.CharField(max_length=30)

    def clean(self):
        index_number = self.cleaned_data.get('index_number')

        if not User.objects.filter(username=index_number):
            raise forms.ValidationError("Index Number is NOT Registered")

        return super(IndexNUmberForm, self).clean()
