from django.conf.urls import url
from .views import (
    StudentVoterListView,
    StudentVoterCreateView,
    student_voter_detail_view,
    StudentVoterDeleteView,
    StudentVoterUpdateView,
    )

urlpatterns = [
    url(r'^$', StudentVoterListView.as_view(), name='home'),
    url(r'^add-new/', StudentVoterCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', student_voter_detail_view, name='detail'),
    url(r'^(?P<pk>\d+)/update/', StudentVoterUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', StudentVoterDeleteView.as_view(), name='delete'),
    # url(r'^index-number-form/$', index_number_view, name='index_number')
]
