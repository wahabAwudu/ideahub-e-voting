from django.contrib import admin

from .models import FO


class FOAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'status', 'created_at','updated_at']
    model = FO


admin.site.register(FO, FOAdmin)
