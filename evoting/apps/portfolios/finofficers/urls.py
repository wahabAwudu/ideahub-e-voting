from django.conf.urls import url
from .views import FOListView, FOCreateView, FODetailView, FODeleteView, FOUpdateView

urlpatterns = [
    url(r'^$', FOListView.as_view(), name='home'),
    url(r'^add-new/', FOCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', FODetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/', FOUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', FODeleteView.as_view(), name='delete'),
]
