from django.shortcuts import render, get_object_or_404, HttpResponse, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView

from .models import FO


class FOListView(ListView):
    model = FO
    template_name = "admin/portfolios/list.html"

    def get_queryset(self):
        return FO.objects.filter(status='AC').order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(FOListView, self).get_context_data(**kwargs)
        context['page_title'] = "List of Finance Officers"
        context['unique_url'] = "finance-officers"
        return context


class FODetailView(DetailView):
    model = FO
    template_name = "admin/portfolios/detail.html"

    def get_context_data(self, **kwargs):
        context = super(FODetailView, self).get_context_data(**kwargs)
        context['page_title'] = "Finance Officer"
        context['unique_url'] = "finance-officers"
        return context


class FOCreateView(CreateView):
    model = FO
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def dispatch(self, form, *args, **kwargs):
        return super(FOCreateView, self).dispatch(form, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Add New Finance Officer"
        context['unique_url'] = "finance-officers"
        return context


class FOUpdateView(UpdateView):
    model = FO
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Update Finance Officer"
        context['unique_url'] = "finance-officers"
        return context


class FODeleteView(DeleteView):
    model = FO
    template_name = "admin/portfolios/confirm_delete.html"
    success_url = reverse_lazy('finofficer:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Delete Finance Officer"
        context['unique_url'] = "finance-officers"
        return context
