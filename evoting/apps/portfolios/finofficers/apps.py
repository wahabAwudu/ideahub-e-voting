from django.apps import AppConfig


class FinofficerConfig(AppConfig):
    name = 'finofficers'
