from django.contrib import admin

from .models import Wocom


class WocomAdmin(admin.ModelAdmin):
    fields = ['__str__', 'status', 'created_at', 'updated_at']
    model = Wocom


admin.site.register(Wocom, WocomAdmin)
