from django.conf.urls import url
from .views import WocomListView, WocomCreateView, WocomDetailView, WocomDeleteView, WocomUpdateView

urlpatterns = [
    url(r'^$', WocomListView.as_view(), name='home'),
    url(r'^add-new/', WocomCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', WocomDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/', WocomUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', WocomDeleteView.as_view(), name='delete')
]
