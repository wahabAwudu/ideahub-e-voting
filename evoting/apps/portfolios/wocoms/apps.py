from django.apps import AppConfig


class WocomsConfig(AppConfig):
    name = 'wocoms'
