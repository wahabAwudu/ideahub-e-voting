from django.shortcuts import render, get_object_or_404, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView

from .models import Wocom


class WocomListView(ListView):
    model = Wocom
    template_name = "admin/portfolios/list.html"

    def get_queryset(self):
        return Wocom.objects.filter(status='AC').order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(WocomListView, self).get_context_data(**kwargs)
        context['page_title'] = "List of Women Commissioners"
        context['unique_url'] = "wocoms"
        return context


class WocomDetailView(DetailView):
    model = Wocom
    template_name = "admin/portfolios/detail.html"

    def get_context_data(self, **kwargs):
        context = super(WocomDetailView, self).get_context_data(**kwargs)
        context['page_title'] = "Women Commissioner"
        context['unique_url'] = "wocoms"
        return context


class WocomCreateView(CreateView):
    model = Wocom
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def dispatch(self, form, *args, **kwargs):
        return super(WocomCreateView, self).dispatch(form, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Add New Women Commissioner"
        context['unique_url'] = "wocoms"
        return context


class WocomUpdateView(UpdateView):
    model = Wocom
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Update Women Commissioner"
        context['unique_url'] = "wocoms"
        return context


class WocomDeleteView(DeleteView):
    model = Wocom
    template_name = "admin/portfolios/confirm_delete.html"
    success_url = reverse_lazy('wocom:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Delete Women Commissioner"
        context['unique_url'] = "wocoms"
        return context
