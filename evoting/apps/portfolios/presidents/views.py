from django.shortcuts import render, get_object_or_404, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView

from .models import President


class PresidentListView(ListView):
    model = President
    template_name = "admin/portfolios/list.html"

    def get_queryset(self):
        return President.objects.filter(status='AC').order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(PresidentListView, self).get_context_data(**kwargs)
        context['page_title'] = "List of Presidents"
        context['unique_url'] = "presidents"
        return context


class PresidentDetailView(DetailView):
    model = President
    template_name = "admin/portfolios/detail.html"

    def get_context_data(self, **kwargs):
        context = super(PresidentDetailView, self).get_context_data(**kwargs)
        context['page_title'] = "President"
        context['unique_url'] = "presidents"
        return context


class PresidentCreateView(CreateView):
    model = President
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def dispatch(self, form, *args, **kwargs):
        return super(PresidentCreateView, self).dispatch(form, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Add New President"
        context['unique_url'] = "presidents"
        return context


class PresidentUpdateView(UpdateView):
    model = President
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Update President"
        context['unique_url'] = "presidents"
        return context

class PresidentDeleteView(DeleteView):
    model = President
    template_name = "admin/portfolios/confirm_delete.html"
    success_url = reverse_lazy('organizer:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Delete President"
        context['unique_url'] = "presidents"
        return context
