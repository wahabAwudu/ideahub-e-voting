from django.contrib import admin

from .models import President


class PresidentAdmin(admin.ModelAdmin):
    fields = ['__str__', 'status', 'created_at', 'updated_at']
    model = President


admin.site.register(President, PresidentAdmin)
