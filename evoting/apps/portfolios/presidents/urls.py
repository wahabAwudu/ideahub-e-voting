from django.conf.urls import url
from .views import PresidentListView, PresidentCreateView, PresidentDetailView, PresidentDeleteView, PresidentUpdateView

urlpatterns = [
    url(r'^$', PresidentListView.as_view(), name='home'),
    url(r'^add-new/', PresidentCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', PresidentDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/', PresidentUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', PresidentDeleteView.as_view(), name='delete')
]
