from django.shortcuts import render, get_object_or_404, HttpResponse, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView

from .models import GenSecretary


class GenSecretaryListView(ListView):
    model = GenSecretary
    template_name = "admin/portfolios/list.html"

    def get_queryset(self):
        return GenSecretary.objects.filter(status='AC').order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(GenSecretaryListView, self).get_context_data(**kwargs)
        context['page_title'] = "List of General Secretaries"
        context['unique_url'] = "general-secretaries"
        return context


class GenSecretaryDetailView(DetailView):
    model = GenSecretary
    template_name = "admin/portfolios/detail.html"

    def get_context_data(self, **kwargs):
        context = super(GenSecretaryDetailView, self).get_context_data(**kwargs)
        context['page_title'] = "General Secretary"
        context['unique_url'] = "general-secretaries"
        return context


class GenSecretaryCreateView(CreateView):
    model = GenSecretary
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def dispatch(self, form, *args, **kwargs):
        return super(GenSecretaryCreateView, self).dispatch(form, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Add New General Secretary"
        context['unique_url'] = "general-secretaries"
        return context


class GenSecretaryUpdateView(UpdateView):
    model = GenSecretary
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Update General Secretary"
        context['unique_url'] = "general-secretaries"
        return context


class GenSecretaryDeleteView(DeleteView):
    model = GenSecretary
    template_name = "admin/portfolios/confirm_delete.html"
    success_url = reverse_lazy('gensecretary:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Delete General Secretary"
        context['unique_url'] = "general-secretaries"
        return context
