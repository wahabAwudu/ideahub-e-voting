from django.contrib import admin

from .models import GenSecretary


class GenSecretaryAdmin(admin.ModelAdmin):
    fields = ['__str__', 'status', 'created_at', 'updated_at']
    model = GenSecretary


admin.site.register(GenSecretary, GenSecretaryAdmin)
