from django.conf.urls import url
from .views import GenSecretaryListView, GenSecretaryCreateView, GenSecretaryDetailView, GenSecretaryDeleteView, GenSecretaryUpdateView

urlpatterns = [
    url(r'^$', GenSecretaryListView.as_view(), name='home'),
    url(r'^add-new/', GenSecretaryCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', GenSecretaryDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/', GenSecretaryUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', GenSecretaryDeleteView.as_view(), name='delete')
]
