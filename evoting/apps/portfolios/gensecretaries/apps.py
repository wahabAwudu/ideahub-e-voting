from django.apps import AppConfig


class GensecretaryConfig(AppConfig):
    name = 'gensecretaries'
