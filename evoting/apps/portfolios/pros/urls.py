from django.conf.urls import url
from .views import PROListView, PROCreateView, PRODetailView, PRODeleteView, PROUpdateView

urlpatterns = [
    url(r'^$', PROListView.as_view(), name='home'),
    url(r'^add-new/', PROCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', PRODetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/', PROUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', PRODeleteView.as_view(), name='delete')
]
