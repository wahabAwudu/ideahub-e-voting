from django.shortcuts import render, get_object_or_404, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView

from .models import PRO


class PROListView(ListView):
    model = PRO
    template_name = "admin/portfolios/list.html"

    def get_queryset(self):
        return PRO.objects.filter(status='AC').order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(PROListView, self).get_context_data(**kwargs)
        context['page_title'] = "List of Pubic Relation Officers"
        context['unique_url'] = "pros"
        return context


class PRODetailView(DetailView):
    model = PRO
    template_name = "admin/portfolios/detail.html"

    def get_context_data(self, **kwargs):
        context = super(PRODetailView, self).get_context_data(**kwargs)
        context['page_title'] = "Public Relation Officer"
        context['unique_url'] = "pros"
        return context


class PROCreateView(CreateView):
    model = PRO
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def dispatch(self, form, *args, **kwargs):
        return super(PROCreateView, self).dispatch(form, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Add New P.R.O"
        context['unique_url'] = "pros"
        return context

class PROUpdateView(UpdateView):
    model = PRO
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Update P.R.O"
        context['unique_url'] = "pros"
        return context

class PRODeleteView(DeleteView):
    model = PRO
    template_name = "admin/portfolios/confirm_delete.html"
    success_url = reverse_lazy('organizer:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Delete P.R.O"
        context['unique_url'] = "pros"
        return context
