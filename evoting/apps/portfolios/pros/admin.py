from django.contrib import admin

from .models import PRO


class PROAdmin(admin.ModelAdmin):
    fields = ['__str__', 'status', 'created_at', 'updated_at']
    model = PRO


admin.site.register(PRO, PROAdmin)
