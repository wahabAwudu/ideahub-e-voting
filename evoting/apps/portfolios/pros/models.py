from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from django.contrib.contenttypes.models import ContentType


class PRO(models.Model):
    contestant = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='pros')
    voters = models.ManyToManyField(settings.AUTH_USER_MODEL, default=1)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    picture = models.ImageField(width_field='width', height_field='height', null=True, blank=True)
    STATUS_OPTIONS = (
        ('AC', 'ACTIVE'),
        ('IN', 'INACTIVE'),
    )
    status = models.CharField(max_length=2, choices=STATUS_OPTIONS)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('pro:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return str(self.contestant)
    
    @property
    def get_content_type(self):
        return ContentType.objects.get_for_model(self.__class__)
