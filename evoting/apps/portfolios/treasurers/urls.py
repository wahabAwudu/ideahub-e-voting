from django.conf.urls import url
from .views import TreasurerListView, TreasurerCreateView, TreasurerDetailView, TreasurerDeleteView, TreasurerUpdateView

urlpatterns = [
    url(r'^$', TreasurerListView.as_view(), name='home'),
    url(r'^add-new/', TreasurerCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', TreasurerDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/', TreasurerUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', TreasurerDeleteView.as_view(), name='delete')
]
