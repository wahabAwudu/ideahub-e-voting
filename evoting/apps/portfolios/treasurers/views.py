from django.shortcuts import render, get_object_or_404, HttpResponse, redirect
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView

from .models import Treasurer


class TreasurerListView(ListView):
    model = Treasurer
    template_name = "admin/portfolios/list.html"

    def get_queryset(self):
        return Treasurer.objects.filter(status='AC').order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(TreasurerListView, self).get_context_data(**kwargs)
        context['page_title'] = "List of Treasurers"
        context['unique_url'] = "treasurers"
        return context


class TreasurerDetailView(DetailView):
    model = Treasurer
    template_name = "admin/portfolios/detail.html"

    def get_context_data(self, **kwargs):
        context = super(TreasurerDetailView, self).get_context_data(**kwargs)
        context['page_title'] = "Treasurer"
        context['unique_url'] = "treasurers"
        return context


class TreasurerCreateView(CreateView):
    model = Treasurer
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def dispatch(self, form, *args, **kwargs):
        return super(TreasurerCreateView, self).dispatch(form, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Add New Treasurer"
        context["unique_url"] = "treasurers"
        return context


class TreasurerUpdateView(UpdateView):
    model = Treasurer
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Update Treasurer"
        context['unique_url'] = "treasurers"
        return context


class TreasurerDeleteView(DeleteView):
    model = Treasurer
    template_name = "admin/portfolios/confirm_delete.html"
    success_url = reverse_lazy('treasurer:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["page_title"] = "Delete Treasurer"
        context['unique_url'] = "treasurers"
        return context
