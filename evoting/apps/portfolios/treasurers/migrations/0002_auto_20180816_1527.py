# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-08-16 15:27
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('treasurers', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='treasurer',
            name='percentage',
        ),
        migrations.RemoveField(
            model_name='treasurer',
            name='votes',
        ),
        migrations.AddField(
            model_name='treasurer',
            name='voters',
            field=models.ManyToManyField(default=1, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='treasurer',
            name='contestant',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='treasurers', to=settings.AUTH_USER_MODEL),
        ),
    ]
