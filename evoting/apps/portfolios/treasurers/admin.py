from django.contrib import admin

from .models import Treasurer


class TreasurerAdmin(admin.ModelAdmin):
    fields = ['__str__', 'status', 'created_at', 'updated_at']
    model = Treasurer


admin.site.register(Treasurer, TreasurerAdmin)
