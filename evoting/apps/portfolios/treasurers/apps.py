from django.apps import AppConfig


class TreasurerConfig(AppConfig):
    name = 'treasurers'
