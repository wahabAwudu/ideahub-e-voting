from django.conf.urls import url
from .views import OrganizerListView, OrganizerCreateView, OrganizerDetailView, OrganizerDeleteView, OrganizerUpdateView

urlpatterns = [
    url(r'^$', OrganizerListView.as_view(), name='home'),
    url(r'^add-new/', OrganizerCreateView.as_view(), name='create'),
    url(r'^detail/(?P<pk>\d+)/', OrganizerDetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/update/', OrganizerUpdateView.as_view(), name='update'),
    url(r'^(?P<pk>\d+)/delete/', OrganizerDeleteView.as_view(), name='delete')
]
