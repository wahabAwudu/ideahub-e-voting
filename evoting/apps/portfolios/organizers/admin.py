from django.contrib import admin

from .models import Organizer


class OrganizerAdmin(admin.ModelAdmin):
    fields = ['__str__', 'status', 'created_at', 'updated_at']
    model = Organizer


admin.site.register(Organizer, OrganizerAdmin)
