from django.shortcuts import render, get_object_or_404, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, DetailView, UpdateView, DeleteView, CreateView

from .models import Organizer


class OrganizerListView(ListView):
    model = Organizer
    template_name = "admin/portfolios/list.html"

    def get_queryset(self):
        return Organizer.objects.filter(status='AC').order_by('-updated_at')

    def get_context_data(self, **kwargs):
        context = super(OrganizerListView, self).get_context_data(**kwargs)
        context['page_title'] = "List of Organizers"
        context['unique_url'] = "organizers"
        return context


class OrganizerDetailView(DetailView):
    model = Organizer
    template_name = "admin/portfolios/detail.html"

    def get_context_data(self, **kwargs):
        context = super(OrganizerDetailView, self).get_context_data(**kwargs)
        context['page_title'] = "Organizer"
        context['unique_url'] = "organizers"
        return context


class OrganizerCreateView(CreateView):
    model = Organizer
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def dispatch(self, form, *args, **kwargs):
        return super(OrganizerCreateView, self).dispatch(form, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Add New Organizer"
        context['unique_url'] = "organizers"
        return context


class OrganizerUpdateView(UpdateView):
    model = Organizer
    template_name = "admin/portfolios/form.html"
    fields = [
        'contestant',
        'picture',
        'status',
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Update Organizer"
        context['unique_url'] = "organizers"
        return context


class OrganizerDeleteView(DeleteView):
    model = Organizer
    template_name = "admin/portfolios/confirm_delete.html"
    success_url = reverse_lazy('organizer:home')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['page_title'] = "Delete Organizer"
        context['unique_url'] = "organizers"
        return context
